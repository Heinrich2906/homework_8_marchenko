package com.sport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heinr on 30.10.2016.
 */
public class Sport2<K extends Discipline, V extends Constituent> {

    //private List<V> constituents = new ArrayList<V>();
    private List<V> constituents;
    private K discipline;

    Sport2(K discipline, List<V> constituents) {
        this.constituents = constituents;
        this.discipline = discipline;
    }

    public void cantLiveWithout() {

        String result = "";
        boolean firstRun;
        firstRun = true;
        String prefix;

        for (Constituent s : constituents) {
            if (firstRun == true) {
                firstRun = false;
                prefix = "";
            } else prefix = ", ";

            result = result + prefix + s.about;
        }

        System.out.println(discipline.description + " it can't live without " + result);
    }
}
