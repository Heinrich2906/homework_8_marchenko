package com.sport;

/**
 * Created by heinr on 20.10.2016.
 */
public class Sport<K extends Discipline, V extends Constituent> {

    private V constituente;
    private K discipline;

    Sport(K discipline, V constituente)

    {
        this.constituente = constituente;
        this.discipline = discipline;
    }

    public void cantLiveWithout() {
        System.out.println(discipline.description + " it can't live without " + constituente.about);
    }
}
