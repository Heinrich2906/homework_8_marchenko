package com.sport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heinr on 29.10.2016.
 */
public class Run {

    public static void main(String[] args) {

        Hockey hockey = new Hockey("Ice hokey", "Winter game description.");
        Football football = new Football("Football", "Game description.");
        Formula formula = new Formula("Formula 1", "Race description.");

        Ball ball = new Ball("Ball is round.");
        Fans fans = new Fans("Passion fans.");
        Ice ice = new Ice("Ice is white.");

        Sport<Football, Ball> footballFirstPassion = new Sport<>(football, ball);
        footballFirstPassion.cantLiveWithout();

        Sport<Football, Fans> footballSecondPassion = new Sport<>(football, fans);
        footballSecondPassion.cantLiveWithout();

        Sport<Hockey, Ice> hockeyFirstPassion = new Sport<>(hockey, ice);
        hockeyFirstPassion.cantLiveWithout();

        Sport<Hockey, Fans> hockeySecondPassion = new Sport<>(hockey, fans);
        hockeySecondPassion.cantLiveWithout();

        Sport<Formula, Fans> formulaFirstPassion = new Sport<>(formula, fans);
        formulaFirstPassion.cantLiveWithout();

        System.out.println("Version 2");

        List<Constituent> constituents = new ArrayList<>();
        constituents.add(ball);
        constituents.add(fans);

        Sport2<Football, Constituent> footballFirstPassion2 = new Sport2<>(football, constituents);
        footballFirstPassion2.cantLiveWithout();

        constituents.clear();
        constituents.add(fans);
        constituents.add(ice);

        Sport2<Hockey, Constituent> hockeyFirstPassion2 = new Sport2<>(hockey, constituents);
        hockeyFirstPassion2.cantLiveWithout();

        constituents.clear();
        constituents.add(fans);

        Sport2<Formula, Constituent> formulaFirstPassion2 = new Sport2<>(formula, constituents);
        formulaFirstPassion2.cantLiveWithout();






    }
}
