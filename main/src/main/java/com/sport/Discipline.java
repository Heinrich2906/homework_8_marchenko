package com.sport;

/**
 * Created by heinr on 20.10.2016.
 */
public class Discipline {

    String description; // описание вида спорта
    String name;        // имя вида спорта

    public String description()    {
        return "It's a " + this.name + ". " + this.description;
    }
}
