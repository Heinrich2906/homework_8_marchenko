package com.company;

/**
 * Created by heinr on 20.10.2016.
 */
public class ZooBox<T extends Animal> {

    public T animal;

    public void lockAnimal(T animal) {
        this.animal = animal;
    }

    public T getAnimal() {
        return animal;
    }
}
