package com.company;

/**
 * Created by heinr on 20.10.2016.
 */
abstract public class Animal {

    public String name;
    public Main.AnimalType animalType;

    public abstract void sayName();
}
