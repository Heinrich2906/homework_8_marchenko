package com.company;

/**
 * Created by heinr on 20.10.2016.
 */
public class Shark extends Animal {

    Shark(String name) {
        this.name = name;
        this.animalType = Main.AnimalType.FISH;
    }

    public void sayName() {
        System.out.println("I'm a " + this.animalType + ". My name's " + this.name);
    }


}
