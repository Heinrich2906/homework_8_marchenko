package com.company;

import java.util.ArrayList;

/**
 * Created by heinr on 20.10.2016.
 */
public class Run {

    public static void main(String[] args) {

        Monkey monkey = new Monkey("Avva");
        Shark shark = new Shark("Tooth");
        Bear bear = new Bear("Mischka");
        Zebra zebra = new Zebra("Koss");

        ZooBox<Monkey> monkeyZooBox = new ZooBox<>();
        ZooBox<Shark> sharkZooBox = new ZooBox<>();
        ZooBox<Bear> bearZooBox = new ZooBox<>();
        ZooBox<Zebra> zebraZooBox = new ZooBox<>();

        monkeyZooBox.lockAnimal(monkey);
        monkey.sayName();

        sharkZooBox.lockAnimal(shark);
        shark.sayName();

        bearZooBox.lockAnimal(bear);
        bear.sayName();

        zebraZooBox.lockAnimal(zebra);
        zebra.sayName();

        ArrayList<ZooBox> zoo = new ArrayList<ZooBox>();

        zoo.add(monkeyZooBox);
        zoo.add(sharkZooBox);
        zoo.add(bearZooBox);
        zoo.add(zebraZooBox);

        for (ZooBox s : zoo) {
            s.animal.sayName();
        }
    }

}

