package com.company;

/**
 * Created by heinr on 20.10.2016.
 */
public class Main {

    enum AnimalType {MAMMAL, AMPHIBIAN, BIRD, FISH}

    ;

    public static void main(String[] args) {
        Bear bear = new Bear("Mischka");
        bear.sayName();
    }
}
