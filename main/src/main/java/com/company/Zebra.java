package com.company;

/**
 * Created by heinr on 20.10.2016.
 */
public class Zebra extends Animal {

    Zebra(String name) {
        this.name = name;
        this.animalType = Main.AnimalType.MAMMAL;
    }

    public void sayName() {
        System.out.println("I'm a " + this.animalType + ". My name's " + this.name);
    }

}
